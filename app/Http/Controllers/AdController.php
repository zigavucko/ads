<?php

namespace App\Http\Controllers;

use App\Ad;
use Illuminate\Http\Request;

/**
 * Controller implements CRUD operations on the data. Its methods are exposed as a JSON API via routes defined in the
 * routed/api file.
 * Model associated with the controller is the app/Ad file.
 * Migrations for table 'ads' reside in the database/migrations directory. To rollback & migrate use the command
 * 'php artisan migrate:refresh'.
 * Seeds are in the database/seeds directory. To seed use the command 'php artisan db:seed'.
 * Tests for JSON API functions reside in the tests/AdTest file. To run tests use the command 'phpunit'.
 *
 * Class AdController
 * @package App\Http\Controllers
 */
class AdController extends Controller {
	/**********
	 * Create *
	 **********/

	/**
	 * Add a single ad.
	 * Request data used for insert is in JSON format.
	 * Response data representing inserted object is in JSON format.
	 *
	 * @param   Request $request
	 * @return  string
	 */
	public function add(Request $request) {
		return Ad::create($request->all())->toJson();
	}

	/********
	 * Read *
	 ********/

	/**
	 * Get a single ad.
	 * Response data representing selected object is in JSON format.
	 *
	 * @param   int $adId
	 * @return  string
	 */
	public function get($adId) {
		return Ad::where(['id' => $adId])->get()->toJson();
	}
	/**
	 * Get all ads.
	 * Response data representing selected objects is in JSON format.
	 *
	 * @return  string
	 */
	public function getAll() {
		return Ad::all()->toJson();
	}

	/**********
	 * Update *
	 **********/

	/**
	 * Update a single ad.
	 * Request data used for update is in JSON format.
	 * Response value indicates failure/success and is as such either 0 or 1.
	 *
	 * @param   Request $request
	 * @param   int     $adId
	 * @return  int
	 */
	public function update(Request $request, $adId) {
		$ad = Ad::find($adId);
		if ($ad === null)
			return 0;
		$ad->update($request->all());
		return 1;
	}

	/**********
	 * Delete *
	 **********/

	/**
	 * Delete a single ad.
	 * Response value indicates failure/success and is as such either 0 or 1.
	 *
	 * @param   int $adId
	 * @return  int
	 */
	public function delete($adId) {
		return Ad::destroy($adId);
	}
}