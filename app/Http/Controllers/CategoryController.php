<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Category;
use Illuminate\Http\Request;

/**
 * Controller implements CRUD operations on the data. Its methods are exposed as a JSON API via routes defined in the
 * routed/api file.
 * Model associated with the controller is the app/Category file.
 * Migrations for table 'categories' reside in the database/migrations directory. To rollback & migrate use the command
 * 'php artisan migrate:refresh'.
 * Seeds are in the database/seeds directory. To seed use the command 'php artisan db:seed'.
 * Tests for JSON API functions reside in the tests/CategoryTest file. To run tests use the command 'phpunit'.
 *
 * Class CategoryController
 * @package App\Http\Controllers
 */
class CategoryController extends Controller {
	/**********
	 * Create *
	 **********/

	/**
	 * Add a single category.
	 * Request data used for insert is in JSON format.
	 * Response data representing inserted object is in JSON format.
	 *
	 * @param   Request $request
	 * @return  string
	 */
	public function add(Request $request) {
		return Category::create($request->all())->toJson();
	}

	/********
	 * Read *
	 ********/

	/**
	 * Get a single category.
	 * Response data representing selected object is in JSON format.
	 *
	 * @param   int $categoryId
	 * @return  string
	 */
	public function get($categoryId) {
		return Category::where(['id' => $categoryId])->get()->toJson();
	}
	/**
	 * Get all categories.
	 * Response data representing selected objects is in JSON format.
	 *
	 * @return  string
	 */
	public function getAll() {
		return Category::all()->toJson();
	}
	/**
	 * Get all ads for the given category.
	 * Response data representing selected objects is in JSON format.
	 *
	 * @param   int $categoryId
	 * @return  string
	 */
	public function getAllAssociatedAds($categoryId) {
		return Ad::where(['category_id' => $categoryId])->get()->toJson();
	}

	/**********
	 * Update *
	 **********/

	/**
	 * Update a single category.
	 * Response value indicates failure/success and is as such either 0 or 1.
	 *
	 * @param   Request $request
	 * @param   int     $categoryId
	 * @return  int
	 */
	public function update(Request $request, $categoryId) {
		$category = Category::find($categoryId);
		if ($category === null)
			return 0;
		$category->update($request->all());
		return 1;
	}

	/**********
	 * Delete *
	 **********/

	/**
	 * Delete a single category.
	 * Response value indicates failure/success and is as such either 0 or 1.
	 *
	 * @param   int $categoryId
	 * @return  int
	 */
	public function delete($categoryId) {
		return Category::destroy($categoryId);
	}
}