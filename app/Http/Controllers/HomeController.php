<?php

namespace App\Http\Controllers;

use App\Category;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller {
	/**
	 * Show all the categories.
	 * Returns a view
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show() {
		return view('home', ['categories' => Category::all()]);
	}
}