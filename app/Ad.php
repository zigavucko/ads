<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model {
	/**
	 * The associated table name.
	 *
	 * @var string
	 */
	protected $table = 'ads';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'category_id', 'name', 'url'
	];
}
