<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	/**
	 * The associated table name.
	 *
	 * @var string
	 */
	protected $table = 'categories';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name'
	];
}
