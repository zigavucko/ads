<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// permanent route parameter constraints are defined in the boot method of RouteServiceProvider

/************
 * Category *
 ************/
Route::get('category/{id}', 'CategoryController@get');
Route::get('category', 'CategoryController@getAll');
Route::get('category/{id}/ad', 'CategoryController@getAllAssociatedAds');

Route::post('category', 'CategoryController@add');

Route::put('category/{id}', 'CategoryController@update');

Route::delete('category/{id}', 'CategoryController@delete');

/******
 * Ad *
 ******/
Route::get('ad/{id}', 'AdController@get');
Route::get('ad', 'AdController@getAll');

Route::post('ad', 'AdController@add');

Route::put('ad/{id}', 'AdController@update');

Route::delete('ad/{id}', 'AdController@delete');