<?php
class AdTest extends TestCase {
	/**
	 * POST & GET & PUT & DELETE JSON API test, which tests the CRUD operations on the 'ads' table.
	 * 1. Creates a 'Category' object and checks if the returned 'Category' object in JSON format contains the request
	 *    data. Extracts the id of the created 'Category' object.
	 * 2. Creates an 'Ad' object and checks if the returned 'Ad' object in JSON format contains the request data.
	 *    Extracts the id of the created 'Ad' object.
	 * 3. Gets the created 'Ad' object in JSON format and checks if it contains the request data.
	 * 4. Updates the created 'Ad' object with new data in JSON format and checks if the returned 'Ad' object in JSON
	 *    format contains the request data.
	 * 5. Deletes the created 'Ad' object.
	 * 6. Deletes the created 'Category' object.
	 *
	 * @return void
	 */
	public function testSimple() {
		// POST (category)
		$request = ['name' => 'Health'];
		$response = $this->json('POST', 'api/category', $request);
		$response->seeJson($request);
		$categoryId = $response->decodeResponseJson()['id'];

		// POST (ad)
		$request = [
			'category_id' => $categoryId,
			'name'        => 'World Health Organization',
			'url'         => 'www.who.int/'
		];
		$response = $this->json('POST', 'api/ad', $request);
		$response->seeJson($request);
		$adId = $response->decodeResponseJson()['id'];

		// GET
		$this->json('GET', 'api/ad/' . $adId)
			->seeJson($request);

		// PUT
		$request = [
			'name' => 'International Committee of the Red Cross',
			'url'  => 'https://www.icrc.org/en'
		];
		$response = $this->json('PUT', 'api/ad/' . $adId, $request)
			->response->getContent();
		$this->assertEquals('1', $response);

		// DELETE (ad)
		$response = $this->json('DELETE', 'api/ad/' . $adId)
			->response->getContent();
		$this->assertEquals('1', $response);

		// DELETE (category)
		$response = $this->json('DELETE', 'api/category/' . $categoryId)
			->response->getContent();
		$this->assertEquals('1', $response);
	}
}
