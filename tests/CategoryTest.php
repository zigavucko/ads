<?php
class CategoryTest extends TestCase {
	/**
	 * POST & GET & PUT & DELETE JSON API test, which tests the CRUD operations on the 'categories' table.
	 * 1. Creates a 'Category' object and checks if the returned 'Category' object in JSON format contains the request
	 *    data.
	 * 2. Extracts the id of the previously created object. Gets the created 'Category' object in JSON format and checks
	 *    if it contains the request data.
	 * 3. Updates the created 'Category' object with new data in JSON format and checks if the returned 'Category'
	 *    object in JSON format contains the request data.
	 * 4. Deletes the created 'Category' object.
	 *
	 * @return void
	 */
	public function testSimple() {
		// POST
		$request = ['name' => 'Health'];
		$response = $this->json('POST', 'api/category', $request);
		$response->seeJson($request);

		// GET
		$id = $response->decodeResponseJson()['id'];
		$this->json('GET', 'api/category/' . $id)
			->seeJson($request);

		// PUT
		$request = ['name' => 'Retail'];
		$response = $this->json('PUT', 'api/category/' . $id, $request)
			->response->getContent();
		$this->assertEquals('1', $response);

		// DELETE
		$response = $this->json('DELETE', 'api/category/' . $id)
			->response->getContent();
		$this->assertEquals('1', $response);
	}
	/**
	 * POST & GET & DELETE JSON API test, which primarily tests the operation that get all ads that are associated with
	 * the given category.
	 * 1. Creates a 'Category' object and checks if the returned 'Category' object in JSON format contains the request
	 *    data. Extracts the id of the created 'Category' object.
	 * 2. Creates an 'Ad' object and checks if the returned 'Ad' object in JSON format contains the request data.
	 *    Extracts the id of the created 'Ad' object.
	 * 3. Gets all the 'Ad' objects that are associated with the 'Category' object in JSON format and checks if the
	 *    content contains the request data.
	 * 4. Deletes the created 'Category' object.
	 * 5. Deletes the created 'Ad' object.
	 *
	 * @return void
	 */
	public function testAssociations() {
		// POST (category)
		$request = ['name' => 'Health'];
		$response = $this->json('POST', 'api/category', $request);
		$response->seeJson($request);
		$categoryId = $response->decodeResponseJson()['id'];

		// POST (ad)
		$request = [
			'category_id' => $categoryId,
			'name'        => 'World Health Organization',
			'url'         => 'www.who.int/'
		];
		$response = $this->json('POST', 'api/ad', $request);
		$response->seeJson($request);
		$adId = $response->decodeResponseJson()['id'];

		// GET
		$this->json('GET', 'api/category/' . $categoryId . '/ad')
			->seeJson($request);

		// DELETE (ad)
		$response = $this->json('DELETE', 'api/ad/' . $adId)
			->response->getContent();
		$this->assertEquals('1', $response);

		// DELETE (category)
		$response = $this->json('DELETE', 'api/category/' . $categoryId)
			->response->getContent();
		$this->assertEquals('1', $response);
	}
}
