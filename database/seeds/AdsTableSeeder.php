<?php

use App\Ad;
use App\Category;
use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('ads')->delete();

		$ads = [
			'Automotive'    => [
				'BMW'           => 'https://www.bmwgroup.com/',
				'Mercedes-Benz' => 'https://www.mercedes-benz.com/',
				'Ferrari'       => 'http://www.ferrari.com/',
				'Lamborghini'   => 'https://www.lamborghini.com/',
				'Aston Martin'  => 'http://www.astonmartin.com',
				'McLaren'       => 'http://cars.mclaren.com/'
			],
			'Entertainment' => [
				'Netflix'         => 'https://www.netflix.com/',
				'Rotten Tomatoes' => 'https://www.rottentomatoes.com/',
				'IMDb'            => 'http://www.imdb.com/',
				'Facebook'        => 'https://www.facebook.com',
				'Twitter'         => 'https://twitter.com/?lang=en'
			],
			'Finance'       => [
				'Forbes'    => 'http://www.forbes.com/finance/',
				'Bloomberg' => 'http://www.bloomberg.com/'
			],
			'Politics'      => [
				'Donald Trump'        => 'https://www.donaldjtrump.com/',
				'Barrack Obama'       => 'https://www.barackobama.com/',
				'United Nations'      => 'http://www.un.org/en/',
				'European Commission' => 'http://ec.europa.eu/'
			],
			'Sports'        => [
				'NBA'                   => 'http://www.nba.com/',
				'NHL'                   => 'https://www.nhl.com/',
				'UEFA Champions League' => 'http://www.uefa.com/uefachampionsleague/',
				'Premier Leaue'         => 'https://www.premierleague.com/',
				'FIS Ski Jumping'       => 'http://www.fis-ski.com/ski-jumping/'
			],
			'Technology'    => [
				'Google'    => 'https://www.google.com/',
				'Microsoft' => 'https://www.microsoft.com/',
				'Apple'     => 'http://www.apple.com/',
				'NASA'      => 'https://www.nasa.gov/'
			],
			'Travel'        => [
				'Hostelworld'      => 'http://www.hostelworld.com/',
				'Booking'          => 'http://www.booking.com',
				'TripAdvisor'      => 'https://www.tripadvisor.com/',
				'Travel + Leisure' => 'http://www.travelandleisure.com/'
			]
		];

		foreach ($ads as $categoryName => $categoryAds) {
			$categoryId = Category::where(['name' => $categoryName])->first()->id;

			foreach ($categoryAds as $adName => $adUrl) {
				Ad::create([
					'category_id' => $categoryId,
					'name'        => $adName,
					'url'         => $adUrl
				]);
			}
		}
	}
}
