<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('categories')->delete();

		$categories = ['Automotive', 'Entertainment', 'Finance', 'Politics', 'Sports', 'Technology', 'Travel'];

		foreach ($categories as $category) {
			Category::create([
				'name' => $category
			]);
		}
	}
}
