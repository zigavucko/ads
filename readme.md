#'Ads' - a simple mobile web application for showing demo ads

###WEB SERVICE: Implemented using a PHP-based framework Laravel.

####For storing the data MySQL database was chosen.
- Data model is defined via classes in database/migrations directory. They create two tables: 'categories' and 'ads'. Assignable value for a record in the 'categories' table is only a name, while each record in 'ad' table is defined by a category id, name and a url. Tables are linked via a foreign key: a single category could be ascribed to none or many ads, whereas a single ad can belong only to a single category.
- CategoriesTableSeeder and AdTableSeeder classes in database/seeds directory implement seeding logic - they fill tables with some testing data for easier development. 

####Client serving API receives and sends data in JSON format and allows CRUD operations on the data.
- API routes in routes/api.php file are defined as nested resources. For an example, if you want to retrieve data about all ads in the database, you would simply use 'api/ad' route, whereas if you want a single ad, which has a distinctive id, you would use 'api/ad/{id}' route. 
- Routes bind functions that are implemented in CategoryController and AdController classes of app/Http/Controllers directory. Functions perform CRUD operations on the data.

####Automatic tests validate the correct behavior of the API.
- AdTest and CategoryTest classes in tests directory test the CRUD operations by calling the functions implemented in the controllers.

####Only accessible view by clients is the home view.
- Home page accessible at the '/' route binds the show function in the HomeController class in the app/Http/Controllers directory. The latter function serves the client with the home.blade.php view that resides in the resources/views directory.
- To eliminate the need to make an immediate AJAX call from the client to the web service to retrieve a list of categories, the following data is already passed to the client with the home view.

###CLIENT: A simple HTML5 web application.

####On page load a list of ad categories is displayed.
- Each category represents a collapsible panel in the bootstrap accordion element.
- If no categories were found, an appropriate notice is displayed.

####Category on click event listener calls the web service in asynchronous fashion.
- AJAX call retrieves a list of ads for the given category from the web service and displays it as a collapsible list group under the given category panel. If no ads were found, an appropriate notice is displayed.
- Function bound to the on click event listener implements a 'timestamp' logic. In order not to produce too many calls to the web service, a data attribute named 'data-ts' was used, which carries a timestamp telling us when the list of ads for the given category was last refreshed via an AJAX call.

####Polling logic calls the web service every one minute and refreshes the data on the client.
- Since multiple clients can modify the data on the back-end side, it is necessary to continuously refresh the displayed data on the front-end side.
- Every minute a call to the web service refreshes the categories displayed on the web site. It updates the names of the existing categories and adds / removes categories if there is a need to do so.
- If a panel of the accordion element is collapsed at the moment when the web service is called, then its list of ads is also refreshed via an AJAX call.

####Automatic tests using jasmine library validate the correct behavior of the client side.
- SpecRunner.html file in the public/jasmine directory runs several tests for the functions implemented in the public/js/util.js file. They include the validation of the update / add / removal of categories, update of the ad list of the collapsed accordion panel and display of the notice when no categories or ads exist. Moreover, it tests the population of the collapsible list group with ad objects and validates the correct behavior of the timestamp logic when refreshing the ad list for the given category.

##INSTALLATION
To setup the project locally use command 'git clone https://zigavucko@bitbucket.org/zigavucko/ads.git' (or via SSH: git@bitbucket.org:zigavucko/ads.git). To view the home page, you have to set 'public' folder as the document root folder of your local server (e.g. Apache). Moreover, you have to have to set up a local database and appropriately configure .env file (which should be a modified copy of the .env.example file). When local database is set up, run 'php artisan migrate --seed' command from the project root directory to create tables and fill them with testing data.

##TESTING
- To run server-side tests use command 'phpunit' in the project root directory.
- To run client-side tests open file public/jasmine/SpecRunner.html in a browser.