const REFRESH_INTERVAL = 60 * 1000; // ms

/**
 * Polling logic for calling the web service and obtaining a list of all the categories and adding them to /
 * removing them from the accordion or updating their title.
 * Web service is called every one minute.
 */
$(document).ready(function () {
    setInterval(getCategories, REFRESH_INTERVAL);
});
function getCategories() {
    callAPI('category').done(function (categories) {
        refreshCategoryList(categories);
    }).fail(reloadPage);
}
function refreshCategoryList(categories) {
    var error = $('#error');
    var accordion = $('#accordion');

    // if request was successful and some data was returned, we refresh the panels attached to the accordion
    if (categories.length > 0) {
        var i;

        // put existing list of category panels into a map for faster retrieval of elements
        var elementList = $('.panel.panel-default');
        var elementMap = {};
        for (i = 0; i < elementList.length; i++)
            elementMap[elementList[i].id] = elementList[i];

        // update existing or add new categories
        for (i = 0; i < categories.length; i++)
            updateOrAddCategory(accordion, elementMap, categories[i]);

        // remove any currently existing category panels that were not included in the response from DOM
        removeNonExistentCategories(elementMap);

        error.hide();
        accordion.show();
    }
    // otherwise, we display the notice that there are not any categories in the database and hide the accordion
    else {
        error.html('No categories have been found.').show();
        accordion.hide();
    }
}
function updateOrAddCategory(accordion, elementMap, category) {
    var element = category['id'] in elementMap ? elementMap[category['id']] : null;

    if (element) {
        // update existing category element and remove its object from the element map
        $(element).find('.category').html(category['name']);
        delete elementMap[element.id];

        // refresh the list of ads if the given categories' panel is currently opened (dropped down)
        refreshCollapsedCategoryPanel(category);
    }
    else {
        // add a so far non-existent category element
        accordion.append('<div id="' + category['id'] + '" class="panel panel-default">' +
            '<div class="panel-heading">' +
            '<h4 class="panel-title">' +
            '<a data-toggle="collapse" data-parent="#accordion" ' +
            'data-id="' + category['id'] + '" data-ts="" ' +
            'href="#collapse' + category['id'] + '" ' +
            'onclick="refreshAdList(this)" class="category">' +
            category['name'] +
            '</a>' +
            '</h4>' +
            '</div>' +
            '<div id="collapse' + category['id'] + '" class="panel-collapse collapse" ' +
            'data-id="' + category['id'] + '"></div>' +
            '</div>');
    }
}
function removeNonExistentCategories(elementMap) {
    Object.keys(elementMap).map(function (id) {
        $(elementMap[id]).remove()
    });
}
function refreshCollapsedCategoryPanel(category) {
    if ($('#collapse' + category['id']).hasClass('in'))
        refreshAdList(category['id']);
}

/**
 * Callback logic for calling the web service and obtaining a list of all the ads for the given category and displaying
 * them in a collapsible list group.
 * Web service is called if time difference between now and last call is greater than one minute.
 */
function getAdsForCategory(element) {
    var categoryId = element.getAttribute('data-id');
    var timestamp = element.getAttribute('data-ts');

    if (!timestamp) {
        refreshAdList(categoryId);
        element.setAttribute('data-ts', new Date().toISOString());
    } else if ((new Date() - new Date(timestamp)) > REFRESH_INTERVAL) {
        refreshAdList(categoryId);
        element.setAttribute('data-ts', new Date().toISOString());
    }
}
function refreshAdList(categoryId) {
    callAPI('category/' + categoryId + '/ad').done(function (ads) {
        populateAdList(categoryId, ads);
    }).fail(reloadPage);
}
function populateAdList(categoryId, ads) {
    var content = '<ul class="list-group">';
    if (ads.length > 0) {
        // if some data was returned, we populate the collapsible list group with ad objects
        for (var i = 0; i < ads.length; i++) {
            content += '<li class="list-group-item ' + (i % 2 == 0 ? 'row-white' : 'row-gray') + '">';
            content += '<a href="' + ads[i]['url'] + '" target="_blank">' + ads[i]['name'] + '</a>';
            content += '</li>';
        }
    }
    else {
        // otherwise, we display the notice that there are not any ads for the given category in the database
        content += '<li class="list-group-item">No ads found.</li>';
    }
    content += '</ul>';

    $('#collapse' + categoryId).html(content);
}

/**
 * Error functions.
 */
function reloadPage() {
    alert('Error occurred while trying to obtain data from the server. Page will be reloaded!');
    location.reload();
}