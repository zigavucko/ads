describe('[Category] Util', function() {
    var container;
    var content;

    beforeAll(function () {
        container = $('#container');
        content = container.html();
    });

    it('should be able to update the name of an existing category and remove its reference from the element map', function () {
        var accordion = $('#accordion');
        var elementList = $('.panel.panel-default');
        var elementMap = {};
        for (var i = 0; i < elementList.length; i++)
            elementMap[elementList[i].id] = elementList[i];
        var category = new Category(1, 'newFoo1');

        updateOrAddCategory(accordion, elementMap, category);

        expect($('#1 .category')).toContainText(category['name']);
        expect(category['id'] in elementMap).toBe(false);
    });

    it('should be able to update ad list of collapsed panel', function() {
        var category = new Category(2, 'newFoo2');
        var spy = spyOn(window, 'refreshAdList');

        refreshCollapsedCategoryPanel(category);

        expect(spy).toHaveBeenCalled();
    });

    it('should be able to add a new category', function () {
        var accordion = $('#accordion');
        var elementList = $('.panel.panel-default');
        var elementMap = {};
        for (var i = 0; i < elementList.length; i++)
            elementMap[elementList[i].id] = elementList[i];
        var category = new Category('3', 'foo3');

        updateOrAddCategory(accordion, elementMap, category);

        expect($('#3')[0]).toBeInDOM();
    });

    it('should be able to remove non-existent categories', function() {
        var element = $('#1')[0];
        var elementMap = {1: element};

        removeNonExistentCategories(elementMap);

        expect(element).not.toBeInDOM();
    });

    it('should be able to display the notice that there are not any categories and hide the accordion', function() {
        var categories = [];

        refreshCategoryList(categories);

        var error = $('#error');
        var accordion = $('#accordion');
        expect(error).toContainText('No categories have been found.');
        expect(error).toBeVisible();
        expect(accordion).toBeHidden();
    });

    afterAll(function () {
        container.html(content);
    });
});