describe('[Ad] Util', function() {
    var container;
    var content;

    beforeAll(function () {
        container = $('#container');
        content = container.html();
    });

    it('should be able to refresh ad list if timestamp is not yet set', function() {
        var element = document.createElement('a');
        element.setAttribute('data-id', '1');
        element.setAttribute('data-ts', '');

        var spy = spyOn(window, 'refreshAdList');

        getAdsForCategory(element);

        expect(spy).toHaveBeenCalled();
    });

    it('should be able to refresh ad list if time difference between the current click and last call is greater than one minute', function() {
        var element = document.createElement('a');
        element.setAttribute('data-id', '1');
        var d1 = new Date();
        var d2 = new Date(d1);
        d2.setMinutes(d1.getMinutes() - 2);
        element.setAttribute('data-ts', d2.toISOString());

        var spy = spyOn(window, 'refreshAdList');

        getAdsForCategory(element);

        expect(spy).toHaveBeenCalled();
    });

    it('should be able to populate the collapsible list group with ad objects', function() {
        var categoryId = 1;
        var ads = [
            new Ad('105', categoryId, 'bar105', 'http://www.bar105.com'),
            new Ad('106', categoryId, 'bar106', 'http://www.bar106.com')
        ];

        populateAdList(categoryId, ads);

        var elementList = $('#1 ul > li');
        expect(elementList).toHaveLength(2);
        for (var i = 0; i < elementList.length; i++)
            expect(elementList[i]).toContainText(ads[i]['name']);
    });

    it('should be able to display the notice that there are not any ads for the given category', function () {
        var categoryId = 1;
        var ads = [];

        populateAdList(categoryId, ads);

        expect($('#1 ul > li')).toContainText('No ads found.');
    });

    afterAll(function () {
        container.html(content);
    });
});