<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ads</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        {{ Html::style('css/bootstrap.min.css') }}
        {{ Html::style('css/styles.css') }}
        {{ Html::script('js/jquery.min.js') }}
        {{ Html::script('js/bootstrap.min.js') }}
        {{ Html::script('js/ie10-viewport-bug-workaround.js') }}
        {{ Html::script('js/ajax.js') }}
        {{ Html::script('js/util.js') }}
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ url('/') }}">Ads</a>
                </div>
            </div>
        </nav>

        <!-- On page load an accordion of collapsible list groups is built. Each accordion element (list group title)
             has an event onclick handler attached to it, which when triggered, calls the web service and obtains a list
             of all ads for the given category and populates the associated collapsible list group. In order not to
             produce too many web service calls, we attach a data attribute named 'data-ts' which carries information
             (timestamp) about when the list of ads for his category was last refreshed via an AJAX call.
             The title itself represents the name of the ad category.
         -->
        <div class="container-fluid">
            @if (count($categories))
                <p id="error" style="display: none"></p>
                <div id="accordion"
                     class="panel-group col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6"
                     style="display: block">
                    @foreach ($categories as $category)
                        <div id="{{ $category->id }}" class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" data-id="{{ $category->id }}"
                                       data-ts=""
                                       href="#collapse{{ $category->id }}" onclick="getAdsForCategory(this)"
                                       class="category">
                                        {{ $category->name }}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{ $category->id }}" class="panel-collapse collapse"></div>
                        </div>
                    @endforeach
                </div>
            @else
                <p id="error" style="display: block">No categories have been found.</p>
                <div id="accordion"
                     class="panel-group col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6"
                     style="display: none"></div>
            @endif
        </div>
    </body>
</html>